import SwiftUI

struct GamePlayView: View {
    
    let gameSentences = Bundle.main.decode("practiceW1.json")
    
    @State public var gameSentence: String = ""
    
    var body: some View {
        TapGameView(tapGameSentence: gameSentence)
            .onAppear(perform: {
                gameSentence = String(gameSentences["sentence1"]?.sentence ?? "I like tanukis.")
            })
    }
}

#Preview {
    GamePlayView()
}
