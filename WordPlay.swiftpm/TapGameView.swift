import SwiftUI


struct TapGameView: View {
    
    @State public var isPlayingGame: Bool = false
    
    var tapGameSentence: String
    @State public var tappedList: [String] = []
    @State public var gameWordsList: [String] = []
    @State public var gameShuffledList: [String] = []
    
    
    var body: some View {
        
        if isPlayingGame == false {
            Text("Tap to Start")
                .onTapGesture(perform: {
                    isPlayingGame.toggle()
                })
        } else {
            VStack {
                Spacer()
                
                HStack {
                    
                    ForEach(gameShuffledList, id: \.self) {word in
                        
                        TapTarget(target: word, isTapped: false)
                            .opacity(tappedList.contains(word) ? 0 : 1)
                            .onTapGesture(perform: {
                                tappedList.append(word)
                                print(tappedList)
                                checkTapOrder()
                            })
                    }
                }
                
                Spacer()
                
                HStack {
                    ForEach(tappedList, id: \.self) {addedWord in
                        
                        TapTarget(target: addedWord, isTapped: false)
                        
                    }
                }
                Spacer()
            }.onAppear(perform: {
                gameShuffledList = shuffleWords() // ["This", "is", "a", "tanuki", "."]
                //print("Checked")
            })
        }
        
        
        
        
    }
    
    func checkTapOrder() {
        for tapItem in tappedList {
            if tappedList.firstIndex(of: tapItem) == gameWordsList.firstIndex(of: tapItem) {
                print("Correct!")
            } else {
                print("Nope")
                tappedList.removeLast()
            }
            if tappedList == gameWordsList {
                isPlayingGame = false
            }
        }
    }
    
    func shuffleWords() -> [String] {
        tappedList.removeAll()
        let endChar = String(tapGameSentence.last ?? ".")
        
        let gameWordsDroppedLast = tapGameSentence.dropLast() // need to make sure there is a . or ? to drop
        print(gameWordsDroppedLast)
        let gamesWordsPlusLast = gameWordsDroppedLast.appending(" " + endChar)
        
        gameWordsList = gamesWordsPlusLast.components(separatedBy: " ") // Seperates the words of the target sentence or word into an array
        
        let gameShuffledList = gameWordsList.shuffled() // Shuffles the words or letters in the array
        print(gameShuffledList)
        return gameShuffledList
        
    }
}






