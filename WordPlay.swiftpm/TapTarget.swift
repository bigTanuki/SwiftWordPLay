import SwiftUI

struct TapTarget: View {
    
    let target: String
    @State var isTapped: Bool
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 25.0)
                .fill(isTapped ? .green : .pink)
                .opacity(0.5)
                .frame(width: 100, height: 50)
            //                .onTapGesture(perform: {
            //                    isTapped.toggle()
            //                })
            Text(target)
        }
        
    }
}

#Preview {
    TapTarget(target: "Tanuki", isTapped: false)
}
